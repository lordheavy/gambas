#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form.terminal 3.19.90\n"
"POT-Creation-Date: 2024-04-13 16:22 UTC\n"
"PO-Revision-Date: 2024-04-04 21:54 UTC\n"
"Last-Translator: benoit <benoit@benoit-desktop>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Gambas Terminal control"
msgstr "Contrôle de terminal pour Gambas"

#: FTestTerminalView.form:46
msgid "B"
msgstr ""

#: FTestTerminalView.form:51
msgid "U"
msgstr ""

#: FTestTerminalView.form:56
msgid "R"
msgstr ""

#: FTestTerminalView.form:61
msgid "D"
msgstr ""

#: FTestTerminalView.form:66
msgid "b"
msgstr ""

#: FTestTerminalView.form:71
msgid "ReadOnly"
msgstr ""

#: FTestTerminalView.form:76
msgid "Test"
msgstr ""

#: PipeTest.form:32
msgid "Send data"
msgstr ""

#: PipeTest.form:37
msgid "Test Data to the terminal"
msgstr ""

#: PipeTest.form:46
msgid "Data From Terminal"
msgstr ""

#: PipeTest.form:51
msgid "kill Session"
msgstr ""

#: PipeTest.form:56
msgid "Task Testing"
msgstr ""

#: PipeTest.form:61
msgid "Task Stop"
msgstr ""

#: PipeTest.form:74
msgid "Start Session"
msgstr ""

#: PipeTest.form:79
msgid "Start Rs232"
msgstr ""

#: PipeTest.form:84
msgid "Stop Rs232"
msgstr ""

#: PipeTest.form:89
msgid "Telnet Start"
msgstr ""

#: PipeTest.form:98
msgid "telNet Stop"
msgstr ""

#: PipeTest.form:103
msgid "127.0.0.1"
msgstr ""

#: PipeTest.form:108
msgid "23"
msgstr ""

#: TerminalView.class:861
msgid "Unable to open selected URL."
msgstr "Impossible d'ouvrir l'URL sélectionnée."

#: TerminalView.class:1712
msgid "Open link"
msgstr "Ouvrir le lien"

#: TerminalView.class:1719
msgid "Copy"
msgstr "Copier"

#: TerminalView.class:1725
msgid "Paste"
msgstr "Coller"

#: TerminalView.class:1733
msgid "Select all"
msgstr "Tout sélectionner"

#: TerminalView.class:1739
msgid "Clear"
msgstr "Effacer"
