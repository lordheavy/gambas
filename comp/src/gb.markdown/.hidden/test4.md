# Gambas Documentation

[[ no-border
==
[/readme]&nbsp;&nbsp;&nbsp;
--
[/license]&nbsp;&nbsp;&nbsp;
--
[Wiki syntax](/doc/wiki)&nbsp;&nbsp;&nbsp;
--
[/translate]&nbsp;&nbsp;&nbsp;
--
[/changes]&nbsp;&nbsp;&nbsp;
--
[Gambas Web Site] (http://gambas.sourceforge.net/)&nbsp;&nbsp;&nbsp;
--
[Bug Tracker] (http://gambaswiki.org/bugtracker)
]]

## Language Reference

<table class="no-border full"><tr><td width="50%">
[/lang] \
[/cat] \
[Native Classes] (/comp/gb) \
[GUI Classes] (/comp/gb.qt4) \
[/comp] \
</td><td width="50%">
[/error] \
[/def] \
[/doc/object-model]
</td></tr></table>

## Documents

Here is a list of [documents] (/doc) and [topics](/topic) that may help you.

<table class="no-border full"><tr><td width="50%">
[/doc/release] \
[/install] \
[/doc/report] \
[/doc/faq] \
[/ide] \
[Some Little Benchmarks] (/doc/benchmark)
</td><td width="50%">
[/doc/diffvb] \
[/tutorial] *Not finished*. \
[/app] \
[/snippets] \
[/howto]
</td></tr></table>

## Developers

This is the [documentation] (/dev) for component developers.

<table class="no-border full"><tr><td width="50%">
[/howto/contribute] \
[/howto/git] \
[/dev/gambas_components] \
</td><td width="50%">
[/dev/gambas] \
[/dev/overview] \
[/dev/api]
</td></tr></table>

## Translators

<table class="no-border full"><tr><td width="50%">
[/howto/translate]
</td></tr></table>

## Packagers

[/howto/package]

<div align="center" style="font-style:italic;">
This documentation is based on a Wiki written in Gambas. \
If you want to modify some pages, you must register on the [bugtracker] (http://gambaswiki.org/bugtracker). \
Once done, you must learn the [Gambas Wiki Markup Syntax](/doc/wiki).

This documentation is under the [Creative Commons Attribution-ShareAlike (CC-BY-SA) 3.0] (http://creativecommons.org/licenses/by-sa/3.0/) license.
</div>
